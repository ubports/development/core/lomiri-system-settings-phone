# Lithuanian translation for lomiri-system-settings-phone
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-system-settings-phone package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-phone\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 16:30+0000\n"
"PO-Revision-Date: 2020-07-08 19:28+0000\n"
"Last-Translator: Arunas <arunas@anakonda.lt>\n"
"Language-Team: Lithuanian <https://translate.ubports.com/projects/ubports/"
"system-settings/lt/>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n % 10 == 1 && (n % 100 < 11 || n % 100 > "
"19)) ? 0 : ((n % 10 >= 2 && n % 10 <= 9 && (n % 100 < 11 || n % 100 > 19)) ? "
"1 : 2);\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2015-07-16 05:41+0000\n"

#. TRANSLATORS: This string will be truncated on smaller displays.
#: ../plugins/phone/CallForwardItem.qml:157
#: ../plugins/phone/CallForwardItem.qml:202
msgid "Forward to"
msgstr ""

#: ../plugins/phone/CallForwardItem.qml:171
msgid "Enter a number"
msgstr "Įveskite numerį"

#: ../plugins/phone/CallForwardItem.qml:218
msgid "Call forwarding can’t be changed right now."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:45 ../plugins/phone/MultiSim.qml:54
#: ../plugins/phone/NoSims.qml:28 ../plugins/phone/SingleSim.qml:42
msgid "Call forwarding"
msgstr "Skambučių peradresavimas"

#: ../plugins/phone/CallForwarding.qml:131
msgid "Forward every incoming call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:146
msgid "Redirects all phone calls to another number."
msgstr "Peradresuoja visus telefono skambučius į kitą numerį."

#: ../plugins/phone/CallForwarding.qml:158
msgid "Call forwarding status can’t be checked "
msgstr ""

#: ../plugins/phone/CallForwarding.qml:166
msgid "Forward incoming calls when:"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:175
msgid "I’m on another call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:186
msgid "I don’t answer"
msgstr "Aš neatsiliepiu"

#: ../plugins/phone/CallForwarding.qml:197
msgid "My phone is unreachable"
msgstr "Mano telefonas yra nepasiekiamas"

#: ../plugins/phone/CallForwarding.qml:227
msgid "Contacts…"
msgstr "Adresatai…"

#: ../plugins/phone/CallForwarding.qml:240
msgid "Cancel"
msgstr "Atsisakyti"

#: ../plugins/phone/CallForwarding.qml:253
msgid "Set"
msgstr "Nustatyti"

#: ../plugins/phone/CallForwarding.qml:274
msgid "Please select a phone number"
msgstr "Pasirinkite telefono numerį"

#: ../plugins/phone/CallForwarding.qml:283
msgid "Numbers"
msgstr "Numeriai"

#: ../plugins/phone/CallForwarding.qml:302
msgid "Could not forward to this contact"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:303
msgid "Contact not associated with any phone number."
msgstr "Adresatas nėra susietas su jokiu telefono numeriu."

#: ../plugins/phone/CallForwarding.qml:305
msgid "OK"
msgstr "Gerai"

#: ../plugins/phone/CallForwarding.qml:382
msgid "All calls"
msgstr "Visi skambučiai"

#: ../plugins/phone/CallForwarding.qml:384
msgid "Some calls"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:386
msgid "Off"
msgstr "Išjungta"

#: ../plugins/phone/CallWaiting.qml:31 ../plugins/phone/CallWaiting.qml:86
#: ../plugins/phone/MultiSim.qml:42 ../plugins/phone/NoSims.qml:34
#: ../plugins/phone/SingleSim.qml:34
msgid "Call waiting"
msgstr ""

#: ../plugins/phone/CallWaiting.qml:101
msgid ""
"Lets you answer or start a new call while on another call, and switch "
"between them"
msgstr ""

#: ../plugins/phone/MultiSim.qml:67 ../plugins/phone/NoSims.qml:42
msgid "Services"
msgstr ""

#: ../plugins/phone/PageComponent.qml:32
msgid "Phone"
msgstr "Telefonas"

#: ../plugins/phone/PageComponent.qml:102
msgid "Dialpad tones"
msgstr ""

#: ../plugins/phone/ServiceInfo.qml:109
#, qt-format
msgid "Last called %1"
msgstr ""

#: ../plugins/phone/ServiceInfo.qml:119
#, fuzzy
msgid "Call"
msgstr "Skambinti"

#. TRANSLATORS: %1 is the name of the (network) carrier
#: ../plugins/phone/Services.qml:36 ../plugins/phone/SingleSim.qml:55
#, qt-format
msgid "%1 Services"
msgstr ""

#: ../plugins/phone/SingleSim.qml:29
msgid "SIM"
msgstr "SIM"

#~ msgid "phone"
#~ msgstr "telefonas"
