# Slovenian translation for lomiri-system-settings-phone
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-phone package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-phone\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 16:30+0000\n"
"PO-Revision-Date: 2020-02-17 19:40+0000\n"
"Last-Translator: Riccardo Riccio <rickyriccio@tiscali.it>\n"
"Language-Team: Slovenian <https://translate.ubports.com/projects/ubports/"
"system-settings/sl/>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || "
"n%100==4 ? 2 : 3;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2015-07-16 05:41+0000\n"

#. TRANSLATORS: This string will be truncated on smaller displays.
#: ../plugins/phone/CallForwardItem.qml:157
#: ../plugins/phone/CallForwardItem.qml:202
msgid "Forward to"
msgstr "Posreduj"

#: ../plugins/phone/CallForwardItem.qml:171
msgid "Enter a number"
msgstr "Vnesite številko"

#: ../plugins/phone/CallForwardItem.qml:218
msgid "Call forwarding can’t be changed right now."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:45 ../plugins/phone/MultiSim.qml:54
#: ../plugins/phone/NoSims.qml:28 ../plugins/phone/SingleSim.qml:42
msgid "Call forwarding"
msgstr "Posredovanje klica"

#: ../plugins/phone/CallForwarding.qml:131
msgid "Forward every incoming call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:146
msgid "Redirects all phone calls to another number."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:158
msgid "Call forwarding status can’t be checked "
msgstr ""

#: ../plugins/phone/CallForwarding.qml:166
#, fuzzy
msgid "Forward incoming calls when:"
msgstr "Za odhodne klice uporabi:"

#: ../plugins/phone/CallForwarding.qml:175
msgid "I’m on another call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:186
msgid "I don’t answer"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:197
#, fuzzy
msgid "My phone is unreachable"
msgstr "ModemManager ni na voljo"

#: ../plugins/phone/CallForwarding.qml:227
msgid "Contacts…"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:240
msgid "Cancel"
msgstr "Prekliči"

#: ../plugins/phone/CallForwarding.qml:253
msgid "Set"
msgstr "Nastavi"

#: ../plugins/phone/CallForwarding.qml:274
#, fuzzy
msgid "Please select a phone number"
msgstr "Telefonska številka"

#: ../plugins/phone/CallForwarding.qml:283
#, fuzzy
msgid "Numbers"
msgstr "številke"

#: ../plugins/phone/CallForwarding.qml:302
msgid "Could not forward to this contact"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:303
msgid "Contact not associated with any phone number."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:305
msgid "OK"
msgstr "V redu"

#: ../plugins/phone/CallForwarding.qml:382
#, fuzzy
msgid "All calls"
msgstr "klic"

#: ../plugins/phone/CallForwarding.qml:384
#, fuzzy
msgid "Some calls"
msgstr "Telefonski klici:"

#: ../plugins/phone/CallForwarding.qml:386
msgid "Off"
msgstr "Onemogočeno"

#: ../plugins/phone/CallWaiting.qml:31 ../plugins/phone/CallWaiting.qml:86
#: ../plugins/phone/MultiSim.qml:42 ../plugins/phone/NoSims.qml:34
#: ../plugins/phone/SingleSim.qml:34
msgid "Call waiting"
msgstr "Dajanje klica na čakanje"

#: ../plugins/phone/CallWaiting.qml:101
msgid ""
"Lets you answer or start a new call while on another call, and switch "
"between them"
msgstr ""
"Vam omogoči, da odgovorite ali začnete nov klic, medtem ko ste na drugem "
"klicu in preklopi med njima"

#: ../plugins/phone/MultiSim.qml:67 ../plugins/phone/NoSims.qml:42
msgid "Services"
msgstr "Storitve"

#: ../plugins/phone/PageComponent.qml:32
msgid "Phone"
msgstr "Telefon"

#: ../plugins/phone/PageComponent.qml:102
#, fuzzy
msgid "Dialpad tones"
msgstr "Zvoki številčnice"

#: ../plugins/phone/ServiceInfo.qml:109
#, qt-format
msgid "Last called %1"
msgstr "Zadnji klic %1"

#: ../plugins/phone/ServiceInfo.qml:119
msgid "Call"
msgstr "Klic"

#. TRANSLATORS: %1 is the name of the (network) carrier
#: ../plugins/phone/Services.qml:36 ../plugins/phone/SingleSim.qml:55
#, qt-format
msgid "%1 Services"
msgstr "%1 storitve"

#: ../plugins/phone/SingleSim.qml:29
msgid "SIM"
msgstr "SIM"

#~ msgid "phone"
#~ msgstr "telefon"

#~ msgid "services"
#~ msgstr "storitve"

#~ msgid "forwarding"
#~ msgstr "posredovanje"

#~ msgid "waiting"
#~ msgstr "čakanje"

#~ msgid "call"
#~ msgstr "klic"

#~ msgid "dialpad"
#~ msgstr "številčnica"

#~ msgid "shortcuts"
#~ msgstr "bližnjice"

#~ msgid "numbers"
#~ msgstr "številke"
