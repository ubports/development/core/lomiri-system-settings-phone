cmake_minimum_required(VERSION 3.16)
project(lomiri-system-settings-phone VERSION 1.1 LANGUAGES C CXX)

if(${PROJECT_BINARY_DIR} STREQUAL ${PROJECT_SOURCE_DIR})
   message(FATAL_ERROR "In-tree build attempt detected, aborting. Set your build dir outside your source dir, delete CMakeCache.txt from source root and try again.")
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
include(FindPkgConfig)
include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5
    COMPONENTS
        Core
        Gui
        DBus
        Network
        Quick
        Qml
    REQUIRED
)

pkg_check_modules(LomiriSystemSettings
    REQUIRED IMPORTED_TARGET
    LomiriSystemSettings
)
pkg_get_variable(PLUGIN_MANIFEST_DIR LomiriSystemSettings plugin_manifest_dir)
pkg_get_variable(PLUGIN_PRIVATE_MODULE_DIR LomiriSystemSettings plugin_private_module_dir)
pkg_get_variable(PLUGIN_MODULE_DIR LomiriSystemSettings plugin_module_dir)
pkg_get_variable(PLUGIN_QML_DIR LomiriSystemSettings plugin_qml_dir)

add_subdirectory(plugins/phone)

add_subdirectory(po)
